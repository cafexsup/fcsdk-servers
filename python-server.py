#!/usr/bin/env python
import json,requests,urlparse
from os import curdir, sep
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

server = "rmorgan-latest.cafex.com"
webAppId = "webapp-id-example"
localPort = 8123

serverUrl = "http://" + server + ":8080/gateway"
fcsdkJsUrl = serverUrl + "/fusion-client-sdk.js"
fcsdkAdapterUrl = serverUrl + "/adapter.js"

def generateSession(user):
    sessionDescription = {
        "webAppId":webAppId,
        "voice":{
            "username":user,
            "domain": "webgateway.example.com"
            }
        }
    payload = json.dumps(sessionDescription)
    headers = { 'Content-Type' : 'application/json' }
    url = "http://" + server + ":8080/gateway/sessions/session"
    response = requests.post(url, data=payload, headers=headers)
    return response.json()["sessionid"]

def replaceTokens(content, sessionKey):
    content = content.replace("%SESSIONID%", sessionKey)
    content = content.replace("%ADAPTER_URL%", fcsdkAdapterUrl)
    content = content.replace("%FCSDK_URL%", fcsdkJsUrl)
    return content

class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            # Ignore requests for favicon
            if self.path.startswith("/favicon.ico"):
                print "Favicon requested, not generating Sesion ID"
                self.send_response(204)
                self.end_headers()
                return

            # Provide a username if they didn't
            user = urlparse.parse_qs(urlparse.urlparse(self.path).query).get('user', None)
            if user is None or user[0] is None:
                print "No username provided, redirecting"
                self.send_response(301)
                self.send_header('Location', '?user=' + userGenerator.next())
                self.end_headers()
                return

            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()

            requestedFile = open(curdir + sep + "session.html")
            body = replaceTokens(requestedFile.read(), generateSession(user[0]))
            self.wfile.write(body)
            requestedFile.close()
            return
        except IOError:
            request.send_error(404,'File Not Found: %s' % request.path)

def getUserGenerator():
    userNumber = 0
    while(True):
        yield "user" + str(userNumber)
        userNumber += 1

userGenerator = getUserGenerator()

try:
    localServer = HTTPServer(('', localPort), RequestHandler)
    print 'Started server on port: ' + str(localPort)
    localServer.serve_forever()

except KeyboardInterrupt:
    print 'Interupt received, shutting down server'
    localServer.socket.close()
