#!/usr/bin/env ruby
require "webrick"
require "json"
require "uri"
require "net/http"

$server = "rmorgan-latest.cafex.com"
$webAppId = "webapp-id-example"
$localPort = 8123

serverUrl = "http://" + $server + ":8080/gateway"
$fcsdkJsUrl = serverUrl + "/fusion-client-sdk.js"
$fcsdkAdapterUrl = serverUrl + "/adapter.js"

$userpart = 0

def getSessionKey(user)
    sessionDescription = {
        "webAppId" => $webAppId,
        "voice" => {
            "username" => user,
            "domain" => "webgateway.example.com"
        }
    }
    url = URI("http://" + $server + ":8080/gateway/sessions/session")

    request = Net::HTTP::Post.new(url.path, { "Content-Type" => "application.json", "Content-Length" => sessionDescription.length().to_s() })
    request.body = sessionDescription.to_json()

    httpClient = Net::HTTP.new(url.host, url.port).start()
    response = httpClient.request(request)

    return JSON.parse(response.body)["sessionid"]
end

def getContent(user)
    contents = File.read("./session.html")
    contents.gsub!("%SESSIONID%", getSessionKey(user))
    contents.gsub!("%ADAPTER_URL%", $fcsdkAdapterUrl)
    contents.gsub!("%FCSDK_URL%", $fcsdkJsUrl)
    return contents
end

class RequestHandler < WEBrick::HTTPServlet::AbstractServlet
    def do_GET (request, response)
        # Chrome requests a icon, so don't generate a session for this
        if request.path.start_with?("/favicon.ico")
            puts "Favicon requested, not generating session ID"
            response.status = 204
            return
        end
        # If they didn't provide a username, redirect them to one
        username = request.query["user"]
        if username.nil? or username.empty?
            puts "No username provided, redirecting"
            $userpart += 1
            # This next method returns instantly
            response.set_redirect(WEBrick::HTTPStatus::Found, "?user=user" + $userpart.to_s())
            return
        end

        response.status = 200
        response.header["Content-type"] = "text/html"
        response.body = getContent(username)
    end
end
 
webserver = WEBrick::HTTPServer.new(:Port => $localPort)
webserver.mount("/", RequestHandler)
 
trap("INT") do
    webserver.shutdown()
end
 
webserver.start()
